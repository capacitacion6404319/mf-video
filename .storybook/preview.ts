import type { Preview, App } from "@storybook/vue3";
import store from '../src/store';
import { setup } from '@storybook/vue3';

import "../src/assets/styles/storybook/_styles.scss";
import "../src/assets/styles/_variables.scss";

setup((app: App) => {
  app.use(store);
})

const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
  },
};

export default preview;
