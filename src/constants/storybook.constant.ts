export const PLANTILLA_HTML = `
<div class="erp-storybook-contenedor">
    <span class="erp-storybook-titulo">$$TITULO$$</span>
    <div class="erp-storybook-cuerpo">
        <div>
            <p class="erp-storybook-resumen erp-storybook-texto">$$SUMMARY$$</p>
            <ul class="erp-storybook-propiedades erp-mt-20"><br>
                $$PROPS$$
            </ul>
            <div style="display:flex; flex-direction: column; align-items: center;justify-content: center;">
                <div class="main-component erp-mt-20">
                    $$COMPONENT$$
                </div>
                <div class="components erp-mt-20">
                    $$COMPONENTS$$
                </div>
            </div>
            <br>
            <div class="erp-mt-10">
                <p class="erp-storybook-resumen erp-storybook-texto"><strong>Autor:</strong> $$AUTOR$$</p>
            </div><br>
            <div class="erp-storybook-historial erp-storybook-texto erp-mt-10">
                <p class="erp-storybook-resumen erp-storybook-texto erp-mb-10"><strong>Historial de modificaciones</strong></p>
                $$HISTORY$$
            </div>
        </div>
    </div>
</div>`;