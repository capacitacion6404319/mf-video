import { defineComponent, onMounted, onUnmounted, ref } from "vue";

export default defineComponent({
    setup() {
        const PREFIX = "cmp-video";
        const videoSrc = ref('URL_DEL_VIDEO');
        const videoPlayer: any = ref(null);

        const playVideo = () => {
            videoPlayer.value.play();
          };

        const handleEventCmpSugerenciaSeleccionarVideo = (event: any) => {
            const detail = event["detail"];
            if (detail) {
                videoSrc.value = detail["item"].video;
                setTimeout(() => {
                    playVideo();
                }, 2000);
            }
        };

        onMounted(() => {
            window.addEventListener('evt-cmp-sugerencia-seleccionar-video', handleEventCmpSugerenciaSeleccionarVideo);
        })

        onUnmounted(() => {
            window.removeEventListener('evt-cmp-sugerencia-seleccionar-video', handleEventCmpSugerenciaSeleccionarVideo);
        })

        return {
            PREFIX,
            videoSrc,
            videoPlayer
        }
    }
})
