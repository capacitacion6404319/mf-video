import { defineComponent } from "vue";

import CmpVideo from "../../components/business/cmp-video/cmp-video.vue";

export default defineComponent({
    components: {
        CmpVideo
    }
})