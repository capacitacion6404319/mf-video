import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

import VwVideo from "../views/vw-video/vw-video.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",    
    component: VwVideo
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
